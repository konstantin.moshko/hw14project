﻿#include <iostream>
#include <string>
#include <windows.h> //для подключения русской раскладки

using namespace std; //что бы кажджый раз не приписывать std::

int main()
{
	//Устанавливаем русскую кодировку в консоль
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	

	//устанавливаем содержимое строки
			//string work{"Мама мыла раму"};
	cout << "Введите фрвзу: \n";
	string work;
	getline(cin, work);
	cout << "\n";

	//выводим строку на экран
	cout << "Строка: " << work << "\n";

	//выводим длину строки
	cout << "Длина строки: " << work.size() << "\n";
	
	//выводим первый символ строки
	cout << "Первый символ: " << *work.begin() << "\n";
	
	//выводим последний символ строки
	int LastChar = work.size();
	cout << "Последний символ: " << work[LastChar-1] << "\n";
	
	return 0;
}